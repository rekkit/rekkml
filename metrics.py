"""
Library with custom metrics
"""

import numpy as np

# root mean square log-loss
def rmsq_log_loss(y, pred):
    y = np.array([np.log(1+ye) for ye in y])
    pred = np.array([np.log(1+pe) for pe in pred])
    # loss
    return np.sqrt(np.sum((y - pred) ** 2) / len(y))

def mean_absolute_error(y, pred):
    return np.mean(np.abs(y - pred))

def mean_absolute_scorer(y, pred):
    return - np.mean(np.abs(y - pred))