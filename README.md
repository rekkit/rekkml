# README #

### What is this repository for? ###
This is a small library where I plan to store all the little pieces of code that I need for data science, which I can't find in other libraries.

### How do I get set up? ###
Just take the rekkml folder and copy it into the directory where you keep your python libraries. If you're using anaconda that would be: C:\Users\User\Anaconda3\Lib\site-packages\rekkml

### Dependencies ###
1. pandas
2. numpy
3. sklearn
4. copy

### Who do I talk to? ###
* vuksa.jovic@gmail.com