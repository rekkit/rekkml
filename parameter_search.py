import pandas as pd
import numpy as np
import copy

"""
Library for handling parameter optimization results
"""

class param_search_res(object):
    """
    Here you input the sklearn random parameter/grid search CV result and initialize the class
    """
    def __init__(self, cv_result):
        self.cv_result = copy.deepcopy(cv_result)

    def get_n_iter(self):
        return len(self.cv_result["params"])

    def get_cv(self):
        return len(self.cv_result["split0_test_score"])

    def get_model_params(self, model_id):
        return copy.deepcopy(self.cv_result["params"][model_id])

    def get_result_dataframe(self):
        test_scores = list()
        model_id = list()
        split_list = list()
        for key in self.cv_result.keys():
            if "split" in key and "_test_score" in key:
                j = 0
                split = int(key.strip("split_test_score"))
                for e in self.cv_result[key]:
                    test_scores.append(e)
                    model_id.append(j)
                    split_list.append(split)
                    j += 1
        #
        return pd.DataFrame.from_dict(dict(model_id=model_id,
                                           split=split_list,
                                           fold_score=test_scores))

    def get_best_model_ids(self, greater_is_better=True):
        if greater_is_better:
            ind = self.get_result_dataframe().groupby("split")["fold_score"].idxmax().values
        else:
            ind = self.get_result_dataframe().groupby("split")["fold_score"].idxmin().values

        return self.get_result_dataframe().loc[ind, "model_id"].values

    def get_best_model_comparison(self, greater_is_better=True):
        ids = self.get_best_model_ids(greater_is_better)
        res = self.get_model_params(ids[0])
        res["model_id"] = ids[0]
        res["model_count"] = 1
        res["mean_test_score"] = self.cv_result["mean_test_score"][ids[0]]
        # make values lists so appending is possible
        for key in res:
            res[key] = [res[key]]
        #
        for i in np.arange(1, len(ids)):
            if ids[i] in res["model_id"]:
                res["model_count"][res["model_id"].index(ids[i])] += 1
            else:
                res["model_id"].append(ids[i])
                res["model_count"].append(1)
                res["mean_test_score"].append(self.cv_result["mean_test_score"][ids[i]])
                for key in res:
                    if key not in ["model_id", "model_count", "mean_test_score"]:
                        res[key].append(self.get_model_params(ids[i])[key])
        #
        return pd.DataFrame.from_dict(res).sort_values(by="mean_test_score", ascending=False)