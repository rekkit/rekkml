import pandas as pd
import numpy as np
from sklearn import preprocessing

class binary_encode(object):
    """
    :param df: pandas DataFrame
    :param cols: a list of columns or a column name that is to be binary-encoded
    :return: calling encode returns a pandas DataFrame with the selected columns binary-encoded
    """

    def __init__(self, train, cols):
        self.cols = cols if isinstance(cols, list) else [cols]

        self.encoders = dict()
        self.n_cols = dict()
        for col in cols:
            self.encoders[col] = preprocessing.LabelEncoder().fit(train.loc[~train[col].isnull(), col].values)
            self.n_cols[col] = len(str(bin(len(train.loc[~train[col].isnull(), col].unique()) - 1))[2:])


    def encode(self, df):
        """
        :param df: pandas DataFrame that needs to be encoded
        :return: pandas DataFrame with the columns specified when the class was intialized now in binary form
        """

        temp = df.copy()

        for col in self.cols:
            #
            print("Encoding column \"{}\".".format(col))

            # get the max number of digits this column will have in binary representation and
            # create a dictionary that will hold the encoded values
            #
            max_digits = self.n_cols[col]

            # use sklearn's encoder to encode the variables in the column
            #
            encoder = self.encoders[col]
            temp.loc[~df[col].isnull(), col] = encoder.transform(df.loc[~df[col].isnull(), col])
            temp[col] = temp[col].apply(lambda x: "NaN" if np.isnan(x) else str(bin(int(x)))[2:].zfill(max_digits)).values

            for i in np.arange(0, max_digits):
                temp["{}_{}".format(col, i)] = temp[col].apply(lambda x: np.NaN if x == "NaN" else float(x[i])).values
                print("Added {}_{}.".format(col, i))

        return temp.drop(self.cols, axis=1)


def check_categorical(df):
    """
    :param df: pandas DataFrame for which you want to check the values of categorical features
    :return: prints all categorical variables as well as their values
    """

    temp = df.dtypes.reset_index().rename(columns={"index": "feature",
                                                   0: "type"})

    for i in np.arange(0, temp.shape[0]):
        if temp.loc[i, "type"] == "object":
            print("The feature \"{}\" has the following categorical values: {}".format(temp.loc[i, "feature"],
                                                                                       df.loc[:, temp.loc[i, "feature"]].unique()))
    del temp
